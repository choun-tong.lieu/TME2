;;;; Exercice 1 : Intervalles

#lang racket

(provide intervalle)

;;; Q1

(define (intervalle m n)
  (if (>= m n) (list)
      (cons m (intervalle (+ m 1) n))))

(define (intervalle-tail m n l)
  (if (>= m n) l
      (intervalle-tail m (- n 1) (cons (- n 1) l))))

;;; Q2

(define l1 (intervalle 1 20))
(define l2 (map (lambda (x) (* x 3)) l1))
l2
(filter (lambda (x) (zero? (modulo x 2))) l2)

;;; Q3

(define (filtermap p? f l)
  (if (pair? l)
      (let ((fcar (f (car l)))
            (fcdr (filtermap p? f (cdr l))))
        (if (p? fcar)
            (cons fcar fcdr)
            fcdr))
      (list)))

(filtermap even? (lambda (x) (* x 3)) (intervalle 1 20))
