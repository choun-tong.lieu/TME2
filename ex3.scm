;;;; Exercice 3 : Tout en pliage

#lang racket

(require "ex1.scm")
(require "ex2.scm")

;;; fold-right: combinateur de réduction <<à droite>>
(define (fold-right f res l)
  (if (pair? l)
      (f (fold-right f res (cdr l)) (car l))
      res))

;;; Q1

(define (rmap f l)
  (fold-right (lambda (acc e) (cons (f e) acc)) (list) l))

(rmap (lambda (x) (* x 2)) (intervalle 1 11))

;; La liste produite est est à l'envers
;; (define (rmap f l)
;;   (myreduce (lambda (acc e) (cons (f e) acc)) (list) l))

;; (reverse (rmap (lambda (x) (* x 2)) (intervalle 1 11)))

;;; Q2

(define (rfilter p? l)
  (fold-right (lambda (acc cur) (if (p? cur) (cons cur acc) acc)) (list) l))

(rfilter even? (intervalle 1 11))
